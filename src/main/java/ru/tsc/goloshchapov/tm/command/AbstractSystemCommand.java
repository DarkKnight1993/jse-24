package ru.tsc.goloshchapov.tm.command;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected void showCommandValue(@Nullable final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

}
