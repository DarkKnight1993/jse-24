package ru.tsc.goloshchapov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.command.AbstractTaskCommand;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.enumerated.Sort;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {
    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            @NotNull Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sortType.getComparator());
        }
        if (tasks == null) return;
        int index = 1;
        System.out.println("[LIST TASKS]");
        for (Task task : tasks) {
            System.out.println(index + ") " + task.toString());
            index++;
        }
        System.out.println("[END LIST]");
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
