package ru.tsc.goloshchapov.tm.exception.entity;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Exception! Project is not found!");
    }

}
