package ru.tsc.goloshchapov.tm.exception.entity;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Exception! Entity is not found!");
    }

    public EntityNotFoundException(String entityType) {
        super("Exception! Entity " + entityType + " is not found!");
    }

}
