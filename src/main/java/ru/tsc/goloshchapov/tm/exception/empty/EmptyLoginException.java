package ru.tsc.goloshchapov.tm.exception.empty;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Exception! Login is empty!");
    }

}
