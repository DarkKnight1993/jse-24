package ru.tsc.goloshchapov.tm.api;

import ru.tsc.goloshchapov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(E entity);

    void remove(E entity);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    E findById(String id);

    E findByIndex(Integer index);

    E removeById(String id);

    E removeByIndex(Integer index);

    void clear();

}
