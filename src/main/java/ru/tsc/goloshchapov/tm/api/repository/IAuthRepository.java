package ru.tsc.goloshchapov.tm.api.repository;

public interface IAuthRepository {

    String getUserId();

    void setUserId(String userId);

    boolean isUserIdExists();

    void clearUserId();

}
