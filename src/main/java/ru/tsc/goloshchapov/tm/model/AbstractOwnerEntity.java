package ru.tsc.goloshchapov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public abstract class AbstractOwnerEntity extends AbstractEntity {

    @NotNull
    protected String userId;

}
